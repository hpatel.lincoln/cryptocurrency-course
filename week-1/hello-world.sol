// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract HelloWorld {

    event NewMessage(string message);

    address public owner;
    mapping(address => bool) isAllowed;
    mapping(address => string[]) messages;
    string lastMessage;
    address lastAddress;

    constructor() {
        owner = msg.sender;
    }

    modifier IsOwner() {
        require(msg.sender == owner, "Only owner can perform this operation.");
        _;
    }

    modifier IsAllowed() {
        require(isAllowed[msg.sender], "Only approved list of people can perform this operation.");
        _;
    }

    function addUser(address _user) public IsOwner() {
        isAllowed[_user] = true;
    }

    function updateMessage(string calldata _message) public IsAllowed() {
        lastMessage = _message;
        lastAddress = msg.sender;
        messages[msg.sender].push(_message);
        emit NewMessage(_message);
    }

    function getMessage(address user, uint index) public view returns (string memory) {
        uint length = messages[user].length;
        require(index < length, "Index out of bounds");
        return messages[user][index];
    }

    function latestMessage() public view returns (string memory, address) {
        return (lastMessage, lastAddress);
    }
}