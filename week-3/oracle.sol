// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

interface IFootballOracle {
    
    function getScore(uint matchID) 
        external
        returns (bool success, uint score1, uint score2);
}

contract EmitMatchEvent {

    event MatchScore(uint matchID, uint score1, uint score2);

    IFootballOracle public oracle;

    constructor(address _oracle) {
        oracle = IFootballOracle(_oracle);
    }

    function checkScore(uint _matchID) public {
        bool success;
        uint score1;
        uint score2;

        (success, score2, score2) = oracle.getScore(_matchID);

        if (success) {
            emit MatchScore(_matchID, score1, score2);
        }
    }
}
