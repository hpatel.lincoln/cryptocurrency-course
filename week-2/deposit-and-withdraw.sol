// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract DepositAndRefund {

    uint constant waitPeriod = 1 minutes;
    string constant phrase = "to the moon";

    mapping(address => uint) balances;
    mapping(address => uint) depositTimes;

    function getBalance(address _party) 
        external 
        view 
        returns(uint)
    {
        // Default balance is 0 if no deposit has been made.
        return balances[_party];
    }

    function deposit() 
        external 
        payable
    {
        require(msg.value > 0, "Must deposit amount bigger than 0.");
        balances[msg.sender] = msg.value;
        depositTimes[msg.sender] = block.timestamp;
    }

    function withdraw(uint _amount) 
        external 
        payable
    {
        require(_amount > 0, "Must withdraw amount bigger than 0.");
        
        require(balances[msg.sender] >= _amount, "Not enough funds to withdraw.");
        require(block.timestamp > depositTimes[msg.sender] + waitPeriod, "No withdrawl allowed within 1 minute since the last deposit.");
        
        balances[msg.sender] -= _amount;
        (bool sent, ) = msg.sender.call{value: _amount}("");
        require(sent, "Failed to send.");
    }
    
    function withdrawWithSignature(uint _amount, bytes calldata _signature) 
        external 
        payable
    {
        require(_amount > 0, "Must withdraw amount bigger than 0.");
        
        bytes32 phraseHash = getPhraseHash(phrase);
        bytes32 signedHash = getSignedHash(phraseHash);
        address signer = recoverSigner(signedHash, _signature);
        require(signer == msg.sender, "Signature must be valid to withdraw.");

        require(balances[signer] >= _amount, "Not enough funds to withdraw.");
        require(block.timestamp > depositTimes[signer] + waitPeriod, "No withdrawl allowed within 1 minute since the last deposit.");

        balances[signer] -= _amount;
        (bool sent, ) = signer.call{value: _amount}("");
        require(sent, "Failed to send.");
    }

    function getPhraseHash(string memory _phrase) 
        public 
        pure 
        returns(bytes32) {
        return keccak256(abi.encodePacked(_phrase));
    }

    function getSignedHash(bytes32 _messageHash) 
        internal 
        pure 
        returns(bytes32)
    {
        return keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", _messageHash));
    }

    function recoverSigner(bytes32 _message, bytes calldata _signature)
        internal
        pure
        returns(address)
    {
        (bytes32 r, bytes32 s, uint8 v) = splitSignature(_signature);
        return ecrecover(_message, v, r, s);
    }

    function splitSignature(bytes memory _signature)
        internal
        pure
        returns(bytes32, bytes32, uint8)
    {
        require(_signature.length == 65, "Invalid signature length.");

        bytes32 r;
        bytes32 s;
        uint8 v;

        assembly {
            // first 32 bytes, after the length prefix
            r := mload(add(_signature, 32))
            // second 32 bytes
            s := mload(add(_signature, 64))
            // final byte (first byte of the next 32 bytes)
            v := byte(0, mload(add(_signature, 96)))
        }

        return (r, s, v);
    }
}
